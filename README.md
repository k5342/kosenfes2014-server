# Kosenfes2014-server
2014年度 第40回 高専祭のメイン部署企画のゲーム管理サーバ。
デバイス認識の仕様で、Macでのみ起動します。(Mavericks で動作確認済)

## 必要な環境
* Ruby (ruby 2.1.1p76 で動作確認済)

## 導入方法
Git からソースをダウンロードします。
```shell
$ git clone https://k5342@bitbucket.org/k5342/kosenfes2014-server.git
```

## 起動
以下のコマンドを実行します。
```shell
$ ruby main.rb
```
## バージョン履歴
* Version 2.0 (2014-10-31)
** 接続モデルを修正。
** Webインターフェイスを追加。
