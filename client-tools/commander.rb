require 'socket'
require 'json'
require 'readline'
require 'pp'

Thread.abort_on_exception = true

$s = TCPSocket.new('localhost', 22154)

def write(dat)
  puts dat.to_json
  $s.puts(dat.to_json)
end

def parse cmd
  puts cmd
  splt = cmd.split(' ')
  case splt[0] 
  when 'l'
    write({
      id: splt[1].to_i
    })
  when 'ac'
    write({
      type: 'activation',
    })
  when 'rd'
    write({
      type: 'ready',
    })
  when 'hit'
    write({
      type: 'action',
      body: {
        type: 'hit',
        amount: splt[1].to_i,
      },
    })
  when 'attack'
    write({
      type: 'action',
      body: {
        type: 'attack',
        target: splt[1].to_i,
        amount: splt[2].to_i,
      },
    })
  when 'magic'
    # magic id status
    splt[2] = 
      case splt[2]
      when /^po/
        'poison'
      when /^pa/
        'paralysis'
      else
        splt[2]
      end
    
    write({
      type: 'action',
      body: {
        type: 'magic',
        target: splt[1],
        status: splt[2],
      },
    })
  else
    puts 'Usage          :'
    puts 'Login          : l <id>'
    puts 'Activation     : ac'
    puts 'Attack by magic: magic <id> <status>'
  end
end

send = Thread.new do
  loop do
    parse(Readline.readline('> ', true))
  end
end

while $s.gets
  puts $_
  begin
    hash = JSON.parse($_)
  rescue
  end
  pp hash
end

send.join
