require './game/main'

Thread.abort_on_exception = true
#Game::Sound.disable = true

begin
  Game.init
  loop do
    Game.player_activation
    Game.choose_items
    Game.play_before
    
    Game.play
    sleep 3
    Game.reset
  end
rescue Interrupt
  Game.shutdown
end
