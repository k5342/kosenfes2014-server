module Game
  module Timer
    class << self
      def start
        @timer = Time.now
      end
      def remain
        @timer ||= Time.now - 90
        t = ((@timer + 90) - Time.now).to_i
        return 0 if t <= 0
        return t
      end
      def remain_percent
        @timer ||= Time.now - 90
        t = ((@timer + 90) - Time.now).to_i
        return 0 if t <= 0
        return 1.0 * t/90
      end
    end
  end
end
