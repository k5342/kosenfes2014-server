require 'sqlite3'

module Game
  class Ranking
    class << self
      def last_time
        return 0 unless @last_time
        @last_time
      end
      def init
        @db = SQLite3::Database.new(Game::Config.db[:ranking])
        sql = <<SQL
          CREATE TABLE ranking (
            id integer PRIMARY KEY AUTOINCREMENT,
            count integer,
            score integer,
            date integer
          );
          CREATE TABLE detail (
            id integer PRIMARY KEY AUTOINCREMENT,
            ranking_id integer NOT NULL,
            score integer
          );
SQL
        @db.execute_batch(sql) unless table_exist?
      end
      def push score_array
        dat = calculate_scores(score_array)
        sql1 = 'INSERT INTO ranking VALUES(null, ?, ?, ?);'
        sql2 = <<SQL
          SELECT
            (SELECT
              COUNT(b.score)
              FROM ranking AS b WHERE b.score > a.score)+1
            FROM ranking AS a
            WHERE a.date = ?
SQL
        sql3 = "SELECT COUNT(*) FROM ranking"
        time = Time.now.to_i
        @last_time = time
        @db.execute(sql1, Game::Device::Tablet.activated, dat[0], time)
        
        # 順位を返す
        rank  = @db.execute(sql2, time).flatten.first
        count = @db.execute(sql3).flatten.first
        
        return rank, count
      end
      def each
        data = @db.execute("SELECT * FROM ranking ORDER BY score DESC;")
        data.each_with_index do |row, grade|
          yield [grade+1, *row]
        end
      end
      def close
        @db.close
      end
      
      private
      def table_exist?
        @db.execute("SELECT tbl_name FROM sqlite_master WHERE type == 'table'").flatten.include?("ranking")
      end
      def calculate_scores score_array
        t = []
        sum = 0
        Game::Entity::Player.scores.each do |s|
          p s
          t << s[:score]
          sum += s[:score]
        end
        
        return sum, *t 
      end
    end
  end
end
