require 'timeout'
require 'serialport'

module Game
  module Device
    class Arduino < AbstractDevice
      class << self
        def init
          super
          @registered_device_path ||= []
        end
        def all_up?
          f = true
          @devices.each_value do |d|
            f &&= (d.angle >= 48)
          end
          
          return f
        end
        def worker_start
          @worker ||= Thread.new do
            loop do
              Dir.glob('/dev/tty.usbmodem*').each do |device_path|
                next if @registered_device_path.include?(device_path)
                begin
                  new(device_path)
                  @registered_device_path << device_path
                rescue
                  puts "#{device_path} - Couldn't detect device type. ignored."
                end
              end
              sleep 3
            end
          end
          @worker.wakeup
        end
        def remove(id)
          @registered_device_path.delete(@devices[id].path) if @registered_device_path.include?(@devices[id].path)
          super
        end
        def register(id, device)
          @devices.store(id, device)
          thread = Thread.new do
            begin
              loop do
                d = device.gets
                if d.nil?
                  raise
                else
                  @devices[id].angle = d.to_i if d.chomp =~ /\d+/
                  @queue.push({data: d, source: id})
                end
              end
            rescue
              remove(id)
            end
          end
          @threads.store(id, thread)
          puts "registered: #{device.inspect} #{id}"
        end
      end
      
      attr_reader :path
      attr_accessor :angle
      
      def initialize(path)
        begin
          @handle = SerialPort.new(path, 9600, 8, 1, 0)
          @id     = fetch_arduino_id
          @path   = path
          @angle  = 0
          @tmp    = []
          #@handle.write('n') unless @id == 'm'
          self.class.register(@id, self)
        rescue
          raise
        end
      end
      def write(dat)
        super
        @tmp << dat
        puts "#############{@tmp.inspect}#############"
      end
      
      private
      def fetch_arduino_id
        timeout(3) do
          @handle.write('?')
          loop do
            t = @handle.gets
            return t.chomp.delete('?') if t[0] == '?'
          end
        end
      end
    end
  end
end
