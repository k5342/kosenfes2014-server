require 'device/tablet_connection'

module Game
  module Device
    class Tablet < AbstractDevice
      class << self
        def init
          super
          @activated_tablets = []
        end
        def reset_activation!
          @devices.each_key do |id|
            inactivate id
          end
        end
        def activate id
          return 0 unless registered?(id)
          @activated_tablets.push(id)
          @devices[id].activated = true
        end
        def inactivate id
          puts "inactivate =>#{id}"
          @devices[id].activated = false if @devices.has_key?(id)
          @activated_tablets.delete(id) if @devices.include?(id)
        end
        def activated
          @activated_tablets.size
        end
        def activated? id
          @activated_tablets.include?(id)
        end
        def println_for_activates dat
          @activated_tablets.each do |id|
            println(dat, id)
          end
        end
        def println_for_all_clients dat
          @devices.each_key do |id|
            println(dat, id)
          end
        end
        def receives
          @queue.clear
          raise unless block_given?
          loop do
            t = @queue.pop
            next unless t[:data].has_key?(:type)
            yield t[:data], t[:source]
          end
        end
        def worker_start
          @worker ||= Thread.new do
            loop do
              until active >= 5
                new
              end
            end
          end
          @worker.wakeup
        end
        def register(id, device)
          device.println({
            type: 'config',
            body: Game::Config,
          }.to_json)
          p ({type: 'config', body: Game::Config}.to_json)
          thread = Thread.new do
            begin
              loop do
                d = device.gets
                if d.nil?
                  raise
                else
                  begin
                    hash = JSON.parse(d, {:symbolize_names => true})
                    @queue.push({data: hash, source: id})
                  rescue => e
                    puts "#{e.class}: #{e.message}, #{id}"
                  end
                end
              end
            rescue
              inactivate(id)
              remove(id)
            end
          end
          if registered?(id)
            inactivate(id)
            remove(id)
          end
          @devices.store(id, device)
          @threads.store(id, thread)
          puts "registered: #{device.inspect} #{id}"
        end
      end
      
      attr_accessor :activated
      
      def initialize
        begin
          @handle    = TabletConnection.new
          @id        = @handle.ident[:id]
          @activated = false
          self.class.register(@id, self)
        rescue => e
          puts e.message
          puts e.backtrace
        end
      end
      def source
        @handle.source
      end
      def ip
        @handle.ip
      end
      def active?
        @activated
      end
      def close
        @handle.close
      end
    end
  end
end
