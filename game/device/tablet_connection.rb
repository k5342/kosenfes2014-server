require 'socket'
require 'json'

class TabletConnection
  attr_reader :source_or_ip, :source, :ip, :id
  def initialize
    @@server  ||= TCPServer.new(22154)
    @connection = establish
  end
  def ident
    return {source_or_ip: @source_or_ip, source: @source, ip: @ip, id: @id}
  end
  def println data
    @connection.write("#{data}\n")
  end
  def write data
    @connection.write(data)
  end
  def gets
    @connection.gets
  end
  def close
    @connection.close
  end
  def closed?
    @connection.closed?
  end
  
private
  def establish
    begin
      @connection   = @@server.accept
      @ip           = @connection.peeraddr[3]
      @source_or_ip = @ip
      
      @ident        = JSON.parse(self.gets, {:symbolize_names => true})
      raise ConnectionError, 'Invalid format'        unless @ident.include?(:id)
      raise ConnectionError, 'Invalid client number' unless @ident[:id].between?(0, 4)
      
      @id           = @ident[:id]
      @source       = "Tablet##{@id}"
      @source_or_ip = @source
      
      return @connection
    rescue JSON::ParserError, ConnectionError
      @connection.close
      raise ConnectionError
    rescue
      @connection.close
      raise
    end
  end
  
  class ConnectionError < StandardError; end
end
