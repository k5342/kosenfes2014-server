module Game
  module Device
    class AbstractDevice
      private_class_method :new
      
      class << self
        def init
          @queue   ||= Queue.new
          @devices ||= {}
          @threads ||= {}
        end
        def registered? id
          @devices.has_key?(id)
        end
        def active
          @devices.size
        end
        def remove id
          puts "remove: #{@devices[id].inspect} #{id.inspect}"
          @devices[id].close if !@devices[id].nil? && !@devices[id].closed?
          @devices.delete(id)
          @threads[id].kill  if @threads[id] && @threads[id].alive?
        end
        def remove_all
          @devices.each do |d|
            d.close
          end
          @threads.each do |t|
            t.kill if t.alive?
          end
          @devices, @threads = {}, {}
        end
        def worker_start
        end
        def worker_stop
          @worker.kill if @worker.is_a?(Thread) && @worker.alive?
        end
        def write(data, id)
          return false unless @devices.has_key?(id)
          puts "#{id} => #{data}"
          
          begin
            @devices[id].write(data)
            true
          rescue => e
            puts "#{id} => #{e.class}"
            false
          end
        end
        def println(data, id)
          write("#{data}\n", id)
        end
        def receives
          raise unless block_given?
          loop do
            t = @queue.pop
            yield t[:data], t[:source]
          end
        end
        def list
          @devices.values
        end
      end
      
      def write data
        #puts "write  => #{data.inspect}"
        return false unless @handle
        @handle.write(data)
      end
      def println data
        write("#{data}\n")
      end
      def gets
        @handle.gets
      end
      def closed?
        @handle.closed?
      end
      def close
        @handle.close
      end
      def connected?
        @handle
      end
    end
    class DeviceError < StandardError; end
    class UnknownDeviceError < DeviceError; end
    class DisconnectedError < DeviceError; end
  end
end
