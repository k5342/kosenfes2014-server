module Game
  class Status
    attr_reader :status
    def self.set now
      @@status =
        case now
        when :activation
          'WAITING PLAYERS'
        when :choose_items
          'CHOOSE ITEMS'
        when :play_before
          'INITIALIZE'
        when :play
          'PLAYING'
        when :ending
          'ENDING'
        else
          'UNKNOWN'
        end
      
      Game::Sound.play_mode(now)
      
      Game::Device::Tablet.println_for_all_clients({
        type: 'control',
        body: {
          start: now
        }
      }.to_json)
    end
    def self.status
      @@status
    end
    def self.orochi
      status = {}
      Game::Entity::Orochi.list.each do |o|
        connection = 
          if Game::Device::Arduino.registered?(o.device)
            'Connected'
          else
            'Disconnected'
          end
        status.store(o.device, {
          connection: connection,
          initial_health: o.initial_health,
          health: o.health,
          status: o.status
        })
      end
      
      status
    end
    def self.tablet
      status = Array.new(5)
      5.times do |id|
        d = Game::Device::Tablet.list[id]
        if d && d.connected?
          connection = (d.active?) ? 'Activated' : 'Connected'
          source     = "#{d.source}(#{d.ip})"
        else
          connection = 'Disconnected'
          source     = '#N/A!'
        end
        status[id] = {connection: connection, source: source}
      end
      
      status
    end
    def self.player
      Game::Entity::Player.list
    end
  end
end
