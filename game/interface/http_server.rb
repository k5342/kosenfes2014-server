require 'webrick'

module Game
  module Interface
    class HTTPServer
      class << self
        def init
          @config ||= {
            BindAddress: '0.0.0.0',
            Port: 3000,
            DocumentRoot: 'www',
            DocumentRootOptions: {
              FancyIndexing: false
            },
          }.freeze
          @srv ||= WEBrick::HTTPServer.new(@config) 
          @srv.mount('/', WEBrick::HTTPServlet::ERBHandler, 'www/status.html.erb')
          @srv.mount('/ranking', WEBrick::HTTPServlet::ERBHandler, 'www/ranking.html.erb')
          @srv.mount('/action', ActionServlet)
        end
        def worker_start
          @worker ||= Thread.new do
            init
            @srv.start
          end
          @worker.wakeup
        end
        def worker_stop
          @worker.kill if @worker.is_a?(Thread) && @worker.alive?
        end
      end
      class ActionServlet < WEBrick::HTTPServlet::AbstractServlet
        def do_POST(req, res)
          case req.query["action"]
          when "start"
            Game.pull_start_trigger
          when "stop"
            Game.stop
          when "reset"
            Game.reset
          end
          #TODO: Cause some action and Redirection
          res.set_redirect(WEBrick::HTTPStatus::MovedPermanently, '/')
        end
      end
    end
  end
end
