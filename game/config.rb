module Game
  module Config
    @orochi, @player, @score, @dir, @db, @sound = {}, {}, {}, {}, {}, {}

    class << self
      attr_accessor :orochi, :player, :score, :dir, :db, :sound
      
      def init
        self.orochi = {
          begin_angry:                0.2, # 大蛇が怒り始める残り体力の割合
          angry_increases:              2, # 大蛇が怒ったときの攻撃増加の割合
          duration_paralysis:           8, # 大蛇の麻痺状態の継続時間
          duration_poison:             10, # 大蛇の毒状態の継続時間
          damage_poison:               50, # 大蛇の毒状態のダメージ量
          attack:                      50, # 大蛇がプレイヤに与える基本ダメージ量
        }
        self.player = {
          power_bomb:                 600, # 爆弾の攻撃力
          attack_amplifier:             3, # 攻撃薬の倍増
          duration_attack_amplifier:   15, # 攻撃薬の時間
          power_gun:                  220, # 火縄銃の攻撃力
          power_bow_first:             10, # 弓の攻撃力（1段階）
          power_bow_second:            20, # 弓の攻撃力（2段階）
          power_bow_third:            100, # 弓の攻撃力（3段階）
          paralysis_amplifier:          3, # 麻痺状態の攻撃力増加
        }
        self.dir    = {
          resource: "resources",
          db:       "db",
          sound:    "resources/sound",
        }
        self.db     = {
          ranking:  "#{Game::Config.dir[:db]}/ranking.db"
        }
        self.sound  = {
          activation:   "#{Game::Config.dir[:sound]}/waiting_players.wav",
          choose_items: "#{Game::Config.dir[:sound]}/choose_items.wav",
          play:         "#{Game::Config.dir[:sound]}/play.wav",
        }
      end
      def to_json(*args)
        {
          orochi: self.orochi,
          player: self.player
        }.to_json(*args)
      end
    end
  end
  
  Game::Config.init
end
