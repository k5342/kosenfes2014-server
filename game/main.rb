$:.unshift File.dirname(__FILE__)

require 'config'
require 'device'
require 'entity'
require 'interface'
require 'timer'
require 'sound'
require 'status'
require 'ranking'

Thread.abort_on_exception = true
#Game::Sound.disable = true

module Game
  @@start_trigger = false
  
  class << self
    def config 
      yield Game::Config if block_given?
    end
    def init
      Game::Device::Arduino.init
      Game::Device::Tablet.init
      Game::Entity::Orochi.init
      Game::Entity::Player.init
      Game::Ranking.init
      
      Game::Device::Arduino.worker_start
      Game::Device::Tablet.worker_start
      Game::Interface::HTTPServer.worker_start
    end
    def shutdown
      Game::Device::Arduino.worker_stop
      Game::Device::Tablet.worker_stop
      Game::Interface::HTTPServer.worker_stop
      Game::Sound.stop
    end
    def player_activation
      Game::Status.set(:activation)
      #TODO: この間にアクティベートされたプレイヤーに対してゲームの実行処理を行う
      #      この処理が終了するときに、アクティベートの結果と、次のステップに進むことを送信する
      puts 'Waiting clients...'
      Game::Entity::Player.activation_worker_start
      until can_start?; end
      Game::Entity::Player.activation_worker_stop
      puts 'Will start'
    end
    def choose_items
      Game::Status.set(:choose_items)
      Game::Entity::Player.choose_items_worker_start
      begin
        timeout(60){
          until Game::Entity::Player.all_ready?
          end
        }
      rescue
      end
      Game::Entity::Player.choose_items_worker_stop
    end
    def play_before
      Game::Status.set(:play_before)
      Game::Entity::Orochi.all_winch_initialize
      begin
        timeout(8){
          until Game::Entity::Orochi.all_up?; end
        }
      rescue
      end
    end
    def play
      Game::Status.set(:play)
      #TODO: クライアントからの値を受け取り、エンティティの操作をする
      #      同時にスコアの計算とインターフェイスへの出力を行う
      #      ゲームクリア or 時間切れで終了、終了時に結果を送信する
      sleep 5
      begin
        Game::Entity::Orochi.play_worker_start
        Game::Entity::Player.play_worker_start
        Game::Timer.start
        while Game::Timer.remain > 0; sleep 1; end
        
        msg = 'GameOver'
        result = :defeat
      rescue Game::Over
        msg = 'GameOver'
        result = :defeat
      rescue Game::Cleared
        msg = 'GameCleared'
        result = :victory
        Game::Entity::Player.each do |e|
          e.add_score(2500)
        end
      rescue => e
        puts "Unknown error has occurred:"
        puts e.class
        puts e.backtrace
        
        msg = 'Unknown error has occurred'
        result = :error
      ensure
        Game::Entity::Orochi.play_worker_stop
        Game::Entity::Player.play_worker_stop
      end
      puts "残り時間 => #{Game::Timer.remain}(#{Game::Timer.remain_percent})"
      
      sleep 2
      
      ranking = Game::Ranking.push(Game::Entity::Player.scores)
      Game::Device::Tablet.println_for_activates({
        type: 'result',
        body: {
          result: result,
          players: Game::Entity::Player.scores,
          grade: ranking[0],
          count: ranking[1],
        }
      }.to_json)
      
      sleep 1
      
      Game::Status.set(:ending)
    end
    def reset
      #TODO: エンティティ系のリセット
      Game::Entity::Orochi.reset!
      Game::Entity::Player.reset!
      #Game::Device::Tablet.reset_activation!
      reset_start_trigger
    end
    
    def pull_start_trigger
      return unless Game::Device::Tablet.activated > 0
      @@start_trigger = true
    end
    def reset_start_trigger
      @@start_trigger = false
    end
    def can_start?
      @@start_trigger
    end
    def stop
      Game::Entity::Orochi.all_winch_paralize
    end
  end
  
  class Over < Exception; end 
  class Cleared < Exception; end 
end
