module Game
  module Entity
    class Orochi < AbstractEntity
      attr_reader :angle, :status, :illumination
      
      class << self
        def init
          super
          @led_device = 'm'
          @entities = []
          @alives = 5
          @attack_definitions = [[0, 1], [0, 1, 2], [1, 2, 3], [2, 3, 4], [3, 4]]
          #[1500, 2000, 5000, 2000, 1500].each_with_index do |health, id|
          [3500, 4000, 5000, 4000, 3500].each_with_index do |health, id|
            new(id, (2*health).to_i).set_device(id.to_s)
          end
        end
        def damage(id, amount)
          t = @entities[id].damage(amount)
          @alives -= 1 if t === 0
          return t
        end
        def paralysis?(id)
          @entities[id].paralysis?
        end
        def set_angle(id, degree)
          @entities[id].set_angle(degree)
        end
        def all_up?
          Game::Device::Arduino.all_up?
        end
        def all_winch_initialize
          @entities.each do |d|
            d.winch_initialize
          end
        end
        def all_winch_paralize
          @entities.each do |d|
            d.winch_paralize
          end
        end
        def play_worker_start
          @play_worker_initializer = Thread.new do
            loop do
              @entities.each do |e|
                e.init unless e.initialized?
              end
            end
          end
          @play_worker_receives = Thread.new do
            Device::Arduino.receives do |dat, src|
              dat.chomp!
              
              case entity_id = search_entity_id(src)
              when 'm'
              when 0..4
                set_angle(entity_id, dat) if dat =~ /^\d+$/
              end
            end
          end
          @play_worker_attacker = Thread.new do
            loop do
              @entities.each do |e|
                next if e.paralysis?
                e.attack(Game::Config.orochi[:attack])
                sleep rand(5)+3 
              end
            end
          end
          @play_worker_interval = Thread.new do
            loop do
              @entities.each do |e|
                if e.poison? && !e.dead?
                  e.damage(Game::Config.orochi[:damage_poison])
                  e.status[:poison] -= 1
                  
                  if e.health <= 0
                    e.set_status!(:dead)
                  end
                end
                if e.paralysis? && !e.dead?
                    e.status[:paralysis] -= 1
                    
                    if e.status[:paralysis] == 0
                      e.winch_start
                      e.set_illumination(:normal)
                    end
                end
                if e.dead?
                  e.status[:angry] = false
                  e.status[:paralysis] = 0
                  e.status[:poison] = 0
                end
              end
              sleep 1
            end
          end
        end
        def play_worker_stop
          @play_worker_initializer.kill
          @play_worker_receives.kill
          @play_worker_attacker.kill
          @play_worker_interval.kill
        end
        def led_device
          @led_device
        end
        def attack_definitions
          @attack_definitions
        end
      end
      
      def initialize(id, health)
        @id, @initial_health = id, health
        @health = @initial_health
        @illumination = nil
        
        @device = nil
        
        @initial_status = {
          initialized: false,
          angry: false,
          dead: false,
          paralysis: 0,
          poison: 0,
        }.freeze
        @status = @initial_status.dup
        
        @initial_angle = 0
        @angle = @initial_angle
        
        self.class.register(id, self)
      end
      def to_json(*opt)
        {
          id: @id,
          health: @health,
          initial_health: @initial_health,
          status: @status,
          position: {
            degree: @angle,
          },
        }.to_json(*opt)
      end
      def attack amount
        amount *= Game::Config.orochi[:angry_increases] if self.angry?
        self.class.attack_definitions[self.id].each do |player_id|
          Game::Entity::Player.damage(player_id, amount, self.id)
        end
      end
      def damage amount
        if (@health.to_f / @initial_health) < Game::Config.orochi[:begin_angry] && !self.angry?
          self.set_status!(:angry)
        end
        
        super
      end
      def init
        self.set_illumination(:normal) if self.illumination === :dead && !self.dead?
        
        return false unless Game::Device::Arduino.registered?(self.device)
        if winch_start
          self.set_status!(:initialized)
          true
        else
          false
        end
      end
      def set_status!(status)
        return if dead? && status == :dead
        
        return if dead?
        return if paralysis? && status == :paralysis
        return if angry?     && status == :angry
        
        if status === :dead && !dead?
          winch_down
        elsif status === :paralysis && !paralysis?
          winch_paralize
        end
        
        @status[status] = 
          case status
          when :initialized
            true
          when :angry
            true
          when :dead
            true
          when :paralysis
            Game::Config.orochi[:duration_paralysis]
          when :poison
            Game::Config.orochi[:duration_poison]
          end
        
        puts "Set status for orochi##{id} => #{status}"
        
        set_illumination(status)
        
        return true
      end
      def winch_initialize
        puts 'up'
        Game::Device::Arduino.write('i', self.device)
      end
      def winch_paralize
        puts 'stop'
        Game::Device::Arduino.write('s', self.device)
      end
      def winch_start
        puts 'start'
        Game::Device::Arduino.write('y', self.device)
      end
      def winch_down
        puts 'down'
        Game::Device::Arduino.write('n', self.device)
      end
      def initialized?
        @status[:initialized]
      end
      def angry?
        @status[:angry]
      end
      def paralysis?
        @status[:paralysis] > 0
      end
      def poison?
        @status[:poison] > 0
      end
        
      def reset
        super
        @angle  = @initial_angle
        @status = @initial_status.dup
        winch_down
        self.set_illumination(:dead)
      end
      
      def set_angle degree
        @angle = degree.to_i
      end
      def set_illumination mode
        return false unless Game::Device::Arduino.registered?(self.class.led_device)
        return false if     self.dead? && self.initialized?
        return false if     self.dead? && mode != :normal
        
        byte_int = 
          (@id.to_i << 4) +
          case mode
          when :normal
            0x01
          when :angry
            0x02
          when :paralysis
            0x03
          when :dead
            0x04
          else
            0x01
          end
        
        @illumination = mode
        byte = byte_int.chr
        puts "#{@id}/#{mode}/#{byte_int}/#{byte}"
        Game::Device::Arduino.write(byte, self.class.led_device)
      end
    end
  end
end
