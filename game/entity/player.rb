module Game
  module Entity
    class Player < AbstractEntity
      class << self
        def init
          super
          @alives = 0
          @no_readys = []
          5.times{|id|
            new(id, 500).set_device(id)
          }
        end
        def damage(id, *args)
          @entities[id].damage(*args)
        end
        def add_score(id, amount)
          @entities[id].add_score(amount)
        end
        def set_health!(id, health)
          if @entities[id].health + health <= 0 && !@entities[id].dead?
            @alives -= 1
            @entities[id].set_status!(:dead)
            raise Game::Over if @alives == 0
          end
          return false if @entities[id].health + health > @entities[id].initial_health
          puts "Health: #{id} => #{health}"
          @entities[id].health += health.to_i
        end
        def scores
          tmp = []
          @entities.each do |e|
            tmp << e.score
          end
          tmp
        end
        def all_ready?
          @no_readys.size === 0
        end
        def activation_worker_start
          @activation_worker = Thread.new do
            Game::Device::Tablet.receives do |dat, id|
              if dat[:type] == 'activation'
                @alives += 1
                Game::Device::Tablet.activate(id)
                Game::Device::Tablet.println({
                  type: 'result',
                  body: {
                    message: 'successfully activated'
                  }
                }.to_json, id)
                @no_readys << id
              else
                Game::Device::Tablet.println({
                  type: 'error',
                  body: {
                    message: 'activation mode'
                  }
                }.to_json, id)
              end
            end
          end
        end
        def activation_worker_stop
          @activation_worker.kill
        end
        def choose_items_worker_start
          @choose_items_worker = Thread.new do
            Game::Device::Tablet.receives do |dat, id|
              next unless Game::Device::Tablet.activated?(id)
              if dat[:type] == 'ready'
                @no_readys.delete(id)
                Game::Device::Tablet.println({
                  type: 'result',
                  body: {
                    message: 'successfully registered'
                  }
                }.to_json, id)
              else
                Game::Device::Tablet.println({
                  type: 'error',
                  body: {
                    message: 'choose items mode'
                  }
                }.to_json, id)
              end
            end
          end
        end
        def choose_items_worker_stop
          @choose_items_worker.kill
        end
        def play_worker_start
          @play_worker_receives = Thread.new do
            Game::Device::Tablet.receives do |dat, id|
              #FIXME: クライアントとリクエストが増えると他のクライアントが長く待たされる可能性
              next unless Game::Device::Tablet.activated?(id)
              puts "#{id} => #{dat}"
              
              type, body = dat[:type], dat[:body]
              target     = body[:target].to_i if body && body.has_key?(:target)
              next if type.nil? || body.nil?
              
              case type
              when 'action'
                score = 
                  case body[:type]
                  when 'attack'
                    ampilifier = (Game::Entity::Orochi.paralysis?(id)) ? 1 : Game::Config.player[:paralysis_amplifier]
                    if Game::Entity::Orochi.damage(target, body[:amount] * ampilifier) === 0
                      100 + ((5 - Game::Entity::Orochi.alives) * (2000 - 1590 * Math.atan(3.0 * (1 - Game::Timer.remain_percent) - 0.4)))
                    else
                      100
                    end
                  when 'hit'
                    set_health!(id, (body[:amount].abs)*(-1))
                    -150
                  when 'defense'
                    200
                  when 'magic'
                    Game::Entity::Orochi.set_status!(target, body[:status].to_sym)
                    500 if body[:status]
                  when 'item_use'
                    set_health!(id, body[:health])
                    300
                  end
                add_score(id, score.to_i)
                if Game::Entity::Orochi.alives <= 0
                  raise Game::Cleared
                end
              end
            end
          end
          @play_worker_interval = Thread.new do
            loop do
              Game::Device::Tablet.println_for_activates({
                type: 'status',
                time: Game::Timer.remain,
                body: Game::Entity::Orochi
              }.to_json)
              sleep 1
            end
          end
        end
        def play_worker_stop
          @play_worker_receives.kill
          @play_worker_interval.kill
        end
        def reset!
          super
          @no_readys = []
        end
      end
      
      def initialize(id, health)
        super
        @score = 0
      end
      def to_json(*opt)
        {
          id: self.id,
          health: self.health,
          scores: self.score,
        }.to_json(*opt)
      end
      def damage(amount, source_orochi_head_id)
        return unless Game::Device::Tablet.activated?(self.id)
        Game::Device::Tablet.println({
          type: 'attack',
          body: {
            from: source_orochi_head_id,
            amount: amount,
          },
        }.to_json, self.id)
      end
      def score
        {
          score: @score
        }
      end
      def add_score amount
        @score += amount
      end
      def reset
        super
        @score = 0
      end
    end
  end
end
