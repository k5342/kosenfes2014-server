module Game
  module Entity
    class AbstractEntity
      private_class_method :new
      attr_reader :id, :initial_health, :device
      attr_accessor :health
      
      class << self
        def init
          @device_list_by_id = {}
        end
        def each
          @entities.each do |e|
            yield e
          end
        end
        def reset!
          @alives = 5
          @entities ||= []
          @entities.each do |e|
            e.reset
          end
        end
        def alives
          @alives
        end
        def to_json(*opt)
          @entities.to_json(*opt)
        end
        def status
          @entities ||= []
          @entities
        end
        def set_status!(id, *arg)
          @entities[id].set_status!(*arg) 
        end
        def set_device(id, device_name)
          @device_list_by_id.store(device_name, id)
        end
        def search_entity_id device_name
          @device_list_by_id[device_name] if @device_list_by_id.has_key?(device_name)
        end
        def play_worker_start
        end
        def play_worker_stop
          @play_worker.kill if @play_worker.alive?
          @play_worker = nil
        end
        def register(id, obj)
          @entities ||= []
          @entities[id] = obj
        end
        def list
          @entities
        end
      end
      
      def initialize(id, health)
        @id, @health = id, health
        @device = ''
        
        @initial_health = health.freeze
        @health = @initial_health
        
        @initial_status = {
          dead: false,
        }.freeze
        @status = @initial_status.dup
        
        self.class.register(id, self)
      end
      def set_device device_name
        @device = device_name
        self.class.set_device(self.id, device_name)
      end
      def damage(amount, source_id = nil)
        return false if self.dead?
        if @health - amount.to_i <= 0
          self.set_status!(:dead)
          @health = 0
        else
          @health -= amount.to_i
        end
      end
      def set_status!(status)
        @status[status] = 
          case status
          when :dead
            true
          end
        
        return true
      end
      def dead?
        @status[:dead] && @health <= 0
      end
      
      def reset
        @health = @initial_health
      end
    end
  end
end
