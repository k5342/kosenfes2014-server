require 'systemu'

module Game
  class Sound
    class << self
      attr_accessor :disable
      @disable = false
      
      def play_mode sym
        stop
        return false unless Game::Config.sound.has_key?(sym)
        play Game::Config.sound[sym]
      end
      def play filename
        return if @disable
        stop
        exec_cmd "afplay #{filename}"
      end
      def stop
        return if @pid.nil?
        begin
        Process.kill(:SIGKILL, @pid)
        rescue
        end
        @pid = nil
        `killall afplay`
      end
      
      private
      def exec_cmd cmd
        @pid = Process.spawn("sleep 3; while true; do #{cmd}; done;")
        Process.detach @pid
      end
    end
  end
end
